package com.mobucks.myexampleapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mobucks.androidsdk.enumerations.Gender;
import com.mobucks.androidsdk.interfaces.AdListener;
import com.mobucks.androidsdk.views.InterstitialAdView;

public class InterstitialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interstitial);
        /*
            Create the interstitial view.

            This can also be created by defining the view in the xml
         */
        final InterstitialAdView interstitialAdView = new InterstitialAdView(this);

        /*
            set the placement id
         */
        interstitialAdView.setPlacementId("<placementId>");

        /**
         * Set the type of placementId to group
         */
        interstitialAdView.setPlacementIdTypeToGroup(true);
        /*
            set the uid
         */
        interstitialAdView.setUid("<uid>");
        /*
            set the password
         */
        interstitialAdView.setPassword("<password>");

        /*
            Adding an ad listener so that we get track events like
            when ad is loaded  , failed or clicked
         */
        interstitialAdView.setAdListener(new AdListener<InterstitialAdView>() {
            /*
            Called when ad is successfully loaded
            */
            @Override
            public void onAdloaded(InterstitialAdView interstitialAdView) {
                /*
                    show the ad
                    you can also use interstitialAdView.showWithAutoDismissDelay(5000);
                    that will dismiss interstitial after some time in millis.
                 */

                interstitialAdView.show();
            }

            @Override
            public void onAdFailed(Exception e) {

            }

            @Override
            public void onAdClicked(InterstitialAdView interstitialAdView) {

            }
        });
         /*
            setting the targeting age (optional)
         */
        interstitialAdView.setTargetingAge(20);
         /*
            setting the targeting gender (optional)
         */
        interstitialAdView.setTargetingGender(Gender.MALE);
         /*
            setting the targeting language (optional)
         */
        interstitialAdView.setTargetingLanguage("el");
         /*
            request interstitial ad
         */
        interstitialAdView.loadAd();
    }
}
