package com.mobucks.myexampleapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.mobucks.androidsdk.enumerations.Gender;
import com.mobucks.androidsdk.interfaces.AdListener;
import com.mobucks.androidsdk.views.BannerAdView;

public class BannerActivity extends AppCompatActivity {
    private BannerAdView bannerAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner);
        /*
            Banner view initialized by xml
            placementId and auth credentials are stored in the xml.

            We could create the view programmatically also like this

            BannerAdView bannerAdView = new BannerAdView("<placementId>","<uid>","<password>",this);
         */
        bannerAdView = findViewById(R.id.bannerView);

        /*
            Adding an ad listener so that we get track events like
            when ad is loaded  , failed or clicked
         */
        bannerAdView.setAdListener(new AdListener<BannerAdView>() {
            /*
             Called when ad is successfully loaded
             */
            @Override
            public void onAdloaded(BannerAdView bannerAdView) {
                Log.i("example","ad loaded");
            }
            /*
             Called when ad failed to load
            */
            @Override
            public void onAdFailed(Exception e) {
                Log.e("example","banner error",e);
            }
            /*
             Called when ad is clicked
            */
            @Override
            public void onAdClicked(BannerAdView bannerAdView) {
                Log.i("example","banner clicked");
            }
        });
        /*
             setting preferred  ad size (optional)
         */
        bannerAdView.setAdSize(320,50);

        /*
            setting ad max size (optional)
         */
        bannerAdView.setMaxSize(1024,768);
        /*
            setting interval for auto refresh (optional)
            time is in millis.
         */
        bannerAdView.setAutoRefreshInterval(60000);

        /*
            If true  the banner will reload on activity resume (optional)
         */
        bannerAdView.setShouldReloadOnResume(true);
        /*
            setting the targeting age (optional)
         */
        bannerAdView.setTargetingAge(20);
         /*
            setting the targeting gender (optional)
         */
        bannerAdView.setTargetingGender(Gender.MALE);
         /*
            setting the targeting language (optional)
         */
        bannerAdView.setTargetingLanguage("el");
        /*
            request banner ad
         */
        bannerAdView.loadAd();
    }

     /*
      Inform sdk about the android life cycle
     */
    @Override
    protected void onResume() {
        super.onResume();
        bannerAdView.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bannerAdView.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        bannerAdView.onPause();
    }
}
