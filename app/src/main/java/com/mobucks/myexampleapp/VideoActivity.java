package com.mobucks.myexampleapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.mobucks.androidsdk.enumerations.Gender;
import com.mobucks.androidsdk.interfaces.AdListener;
import com.mobucks.androidsdk.views.VideoAdView;

public class VideoActivity extends AppCompatActivity {
    private VideoAdView videoAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

          /*
            Video view initialized by xml
            placementId and auth credentials are stored in the xml.

            We could create the view programmatically also like this

            VideoAdView videoAdView = new VideoAdView("<placementId>","<uid>","<password>",this);
         */
         videoAdView = findViewById(R.id.videoView);
         /*
            Adding an ad listener so that we get track events like
            when ad is loaded  , failed or clicked
         */
        videoAdView.setAdListener(new AdListener<VideoAdView>() {
            /*
            Called when ad is successfully loaded
            */
            @Override
            public void onAdloaded(VideoAdView videoAdView) {
                Log.i("example","ad loaded");
            }
            /*
            Called when ad failed to load
            */
            @Override
            public void onAdFailed(Exception e) {
                Log.e("example","video error",e);
            }
            /*
             Called when ad is clicked
            */
            @Override
            public void onAdClicked(VideoAdView videoAdView) {
                Log.i("example","video clicked");
            }
        });
         /*
            setting the targeting age (optional)
         */
        videoAdView.setTargetingAge(19);
        /*
            setting the targeting gender (optional)
         */
        videoAdView.setTargetingGender(Gender.MALE);
        /*
            setting the targeting language (optional)
         */
        videoAdView.setTargetingLanguage("el");
        /*
            request video ad
         */
        videoAdView.loadAd();
    }

    /*
        Inform sdk about the android life cycle
     */
    @Override
    protected void onResume() {
        super.onResume();
        videoAdView.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        videoAdView.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoAdView.onPause();
    }
}
