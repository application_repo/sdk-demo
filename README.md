# Mobucks Sdk

Mobucks� enables app owners to embed text, banner, audio, video targeted advertisements to their mobile applications. Initially, inventory placements will be defined and setup together with the Inventory Owner (= slicing & dicing). Each placeholder is uniquely identified by the property �plid� and has configuration regarding the ad serving type (XML, Vast feed etc.), pricing and content rendering. Each premium campaign that is setup via Mobucks can use one or more placeholders to be displayed

# Installation
 Add the lib folder as repository for your project
```gradle
allprojects {
    repositories {
        google()
        jcenter()
        flatDir {
            dirs 'libs'
        }
    }
}
```
Place mobucks-sdk.aar in the lib folder and add the following dependencies in your app build.gradle.
```gradle
implementation group: 'com.appnexus.opensdk', name: 'appnexus-sdk', version: '4.11.2'
implementation group: 'com.appnexus.opensdk', name: 'appnexus-instreamvideo', version: '4.11.2-1.9'
implementation 'com.google.android.exoplayer:exoplayer:2.7.1'
implementation 'com.google.android.gms:play-services-ads:16.0.0'
implementation 'com.google.android.gms:play-services-location:16.0.0'
implementation(name: 'mobucks-sdk', ext: 'aar')
```

#Edit Proguard Settings
If your app uses Proguard, you must edit your Proguard settings to avoid stripping Google Play out of your app. The SDK doesn't require Google Play to function, but you will lose access to the Android Advertiser ID (AAID), which is used for app targeting and frequency capping. Edit your project's proguard-project.txt file to add the following:
```
### Android: Proguard settings to avoid stripping out Google Play
-keep public class com.mobucks.androidsdk.** {public *;}
-keep public interface com.mobucks.androidsdk.* {*;}
-keep public class com.appnexus.opensdk.** {public *;}
-keep public interface com.appnexus.opensdk.* {*;}

-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}
```
# Banners
### Create the banner view
Can be created programmatically like this
```java
BannerAdView bannerAdView = new BannerAdView("<placementId>","<uid>","<password>",this);
```
or be defined in the layout xml like this
```xml
    <com.mobucks.androidsdk.views.BannerAdView
        android:id="@+id/bannerView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:password="<password>"
        app:placementId="<placementId>"
        app:uid="<uid>"">
    </com.mobucks.androidsdk.views.BannerAdView>
```
### Add the ad listener
```java
     /*
            Adding an ad listener so that we get track events like
            when ad is loaded  , failed or clicked
         */
        bannerAdView.setAdListener(new AdListener<BannerAdView>() {
            /*
             Called when ad is successfully loaded
             */
            @Override
            public void onAdloaded(BannerAdView bannerAdView) {
                Log.i("example","ad loaded");
            }
            /*
             Called when ad failed to load
            */
            @Override
            public void onAdFailed(Exception e) {
                Log.e("example","banner error",e);
            }
            /*
             Called when ad is clicked
            */
            @Override
            public void onAdClicked(BannerAdView bannerAdView) {
                Log.i("example","banner clicked");
            }
        });
```
### Optional configuration
```java
 /*
    setting preferred  ad size (optional)
 */
 bannerAdView.setAdSize(320,50);
/*
    setting ad max size (optional)
*/
bannerAdView.setMaxSize(1024,768);
/*
    setting interval for auto refresh (optional)
    time is in millis.
*/
bannerAdView.setAutoRefreshInterval(60000);
/*
    If true  the banner will reload on activity resume (optional)
 */
bannerAdView.setShouldReloadOnResume(true);
/*
    setting the targeting age (optional)
*/
bannerAdView.setTargetingAge(20);
/*
    setting the targeting gender (optional)
*/
bannerAdView.setTargetingGender(Gender.MALE);
/*
    setting the targeting language (optional)
 */
bannerAdView.setTargetingLanguage("el");
```
### Request banner ad
```java
 /*
    request banner ad
 */
bannerAdView.loadAd();
```

Complete example [here](https://bitbucket.org/application_repo/sdk-demo/src/master/app/src/main/java/com/mobucks/myexampleapp/BannerActivity.java)

# Interstitial
### Create the interstitial view
Can be created programmatically like this
```java
InterstitialAdView interstitialAdView = new InterstitialAdView("<placementId>","<uid>","<password>",this);
```
### Add the ad listener
```java
     /*
            Adding an ad listener so that we get track events like
            when ad is loaded  , failed or clicked
         */
        interstitialAdView.setAdListener(new AdListener<InterstitialAdView>() {
            /*
             Called when ad is successfully loaded
             */
            @Override
            public void onAdloaded(InterstitialAdView interstitialAdView) {
                Log.i("example","ad loaded");
            }
            /*
             Called when ad failed to load
            */
            @Override
            public void onAdFailed(Exception e) {
                Log.e("example","interstitial error",e);
            }
            /*
             Called when ad is clicked
            */
            @Override
            public void onAdClicked(InterstitialAdView interstitialAdView) {
                Log.i("example","interstitial clicked");
            }
        });
```
### Optional configuration
```java

/*
    Set the type of placementId to group
*/
  interstitialAdView.setPlacementIdTypeToGroup(true);
 /*
    setting preferred  ad size (optional)
 */
 interstitialAdView.setAdSize(320,50);
/*
    setting the targeting age (optional)
*/
interstitialAdView.setTargetingAge(20);
/*
    setting the targeting gender (optional)
*/
interstitialAdView.setTargetingGender(Gender.MALE);
/*
    setting the targeting language (optional)
 */
interstitialAdView.setTargetingLanguage("el");
```
### Request interstitial ad
```java
 /*
    request interstitial ad
 */
interstitialAdView.loadAd();
```

### Show the ad
You can check if ad is ready with
```java
interstitialAdView.isReady();
```
and use
```java
interstitialAdView.show();
```
to show the ad


Complete example [here](https://bitbucket.org/application_repo/sdk-demo/src/master/app/src/main/java/com/mobucks/myexampleapp/InterstitialActivity.java)

# Video
### Create the video view
Can be created programmatically like this
```java
VideoAdView videoAdView = new VideoAdView("<placementId>","<uid>","<password>",this);
```
or be defined in the layout xml like this
```xml
    <com.mobucks.androidsdk.views.VideoAdView
        android:id="@+id/bannerView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:password="<password>"
        app:placementId="<placementId>"
        app:uid="<uid>"">
    </com.mobucks.androidsdk.views.BannerAdView>
```
### Add the ad listener
```java
/*
            Adding an ad listener so that we get track events like
            when ad is loaded  , failed or clicked
         */
        videoAdView.setAdListener(new AdListener<VideoAdView>() {
            /*
            Called when ad is successfully loaded
            */
            @Override
            public void onAdloaded(VideoAdView videoAdView) {
                Log.i("example","ad loaded");
            }
            /*
            Called when ad failed to load
            */
            @Override
            public void onAdFailed(Exception e) {
                Log.e("example","video error",e);
            }
            /*
             Called when ad is clicked
            */
            @Override
            public void onAdClicked(VideoAdView videoAdView) {
                Log.i("example","video clicked");
            }
        });
```
### Optional configuration
```java
 /*
   setting the targeting age (optional)
 */
videoAdView.setTargetingAge(19);
/*
    setting the targeting gender (optional)
*/
videoAdView.setTargetingGender(Gender.MALE);
/*
    setting the targeting language (optional)
*/
videoAdView.setTargetingLanguage("el");
```
### Request video ad
```java
/*
    request video ad
 */
videoAdView.loadAd();
```
Complete example [here](https://bitbucket.org/application_repo/sdk-demo/src/master/app/src/main/java/com/mobucks/myexampleapp/VideoActivity.java)
